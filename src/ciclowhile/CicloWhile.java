/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclowhile;

/**
 *
 * @author ize
 */
public class CicloWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int contador = 0;

        while (contador <= 5) {
            System.out.println("Contador: " + contador);
            contador++;
        }

        do {
            System.out.println("Contador: " + contador);
            contador++;
        } while (contador <= 6);

        for (contador = 0; contador < 3; contador++) {
            System.out.println("Contador: " + contador);
        }

    }

}
